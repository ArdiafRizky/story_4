from django.shortcuts import render,redirect
from . import forms
from .models import Task,Assignment
# Create your views here.
def get_name(request):
    if(request.method == "POST"):  
        form = forms.NameForm(request.POST, request.FILES)
        if(form.is_valid()):
            a = Assignment(
                name = form.cleaned_data["name"],
                location = form.cleaned_data["location"],
                categories = form.cleaned_data["categories"],
            )
            a.save()
            return redirect("formApp:form_pages")
    else:
        form = forms.NameForm()

    assignment = Assignment.objects.all()    
    return render(request, 'form/formPage.html', {"assignment":assignment, "forms":form})
    # form = forms.NameForm()
    # return render(request, 'form/formPage.html', {'form':form})
    