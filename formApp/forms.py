from django import forms
from . import models

class NameForm(forms.Form):
    name = forms.CharField(label="Name",required=True)
    location = forms.CharField(label="Location",required=False)
    categories = forms.CharField(label="Categories",required=False)
    class Meta:
        model = models.Assignment
        fields = ['name','location','categories']