# Generated by Django 2.2.5 on 2019-10-05 12:53

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('formApp', '0002_auto_20191005_1252'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Assignment',
            new_name='Task',
        ),
    ]
