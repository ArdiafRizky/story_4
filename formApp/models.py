from django.db import models

# Create your models here.
class Task(models.Model):
    name = models.CharField(max_length=100, default="")
    # location = models.CharField(max_length=100, default="")
    # categories = models.CharField(max_length=100, default="")
    # date = models.DateTimeField(max_length=100, auto_now_add=True)

class Assignment(models.Model):
    name = models.CharField(max_length=100, default="")
    location = models.CharField(max_length=100, default="")
    categories = models.CharField(max_length=100, default="")
    date = models.DateTimeField(max_length=100, auto_now_add=True)