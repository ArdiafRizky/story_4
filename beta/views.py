from django.shortcuts import render,redirect
from .models import BetaModel
from .forms import BetaForm
# Create your views here.
def addName(request):
    if request.method=="POST":
        form = BetaForm(request.POST)
        if form.is_valid():
            b = BetaModel(
                name = form.cleaned_data['name'],
                location = form.cleaned_data['location'],
                categories = form.cleaned_data['categories'],
                date = form.cleaned_data['date'],
                )
            b.save()

    form = BetaForm()
    return render(request, 'beta/betaPage.html', {'form':form})

def getDisplay(request):
    beta = BetaModel.objects.all().order_by("date")
    return render(request, 'beta/displayPage.html', {'beta':beta})

def delete(request, id):
    task = BetaModel.objects.get(pk=id)
    task.delete()
    return redirect("beta:display-page")