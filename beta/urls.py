from django.contrib import admin
from django.urls import path,include
from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
from django.conf.urls.static import static

app_name = "beta"

urlpatterns = [
    path('', views.addName, name="beta-page"),
    path('delete/<int:id>', views.delete, name="delete"),
    path('display/', views.getDisplay, name="display-page")
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

urlpatterns += staticfiles_urlpatterns()
