from django.db import models

# Create your models here.
class BetaModel(models.Model):
    name = models.CharField(max_length=100, default="")
    location = models.CharField(max_length=100, default="")
    categories = models.CharField(max_length=100, default="")
    date = models.DateField(max_length=100, default="")