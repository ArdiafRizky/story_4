from django import forms
from .models import BetaModel

class BetaForm(forms.Form):
    name = forms.CharField(required=True)
    location = forms.CharField(required=True)
    categories = forms.CharField(required=True)
    date = forms.DateField(required=True, widget=forms.DateInput(
        attrs={
            "class":"form-control",
            "type":"date",
        }
    ))
    # class Meta:
    #     model = BetaModel
    #     fields = "__all__"